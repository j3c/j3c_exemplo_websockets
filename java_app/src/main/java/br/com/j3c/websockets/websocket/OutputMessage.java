package br.com.j3c.websockets.websocket;

import java.util.Date;

/**
 * @author carlos.oliveira
 * @since 2016-09-17
 */
public class OutputMessage extends Message {

    private Date time;

    public OutputMessage(Message original, Date time) {
        super(original.getId(), original.getMessage());
        this.time = time;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

}
