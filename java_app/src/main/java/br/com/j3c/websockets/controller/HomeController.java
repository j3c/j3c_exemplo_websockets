package br.com.j3c.websockets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author carlos.oliveira
 * @version 1.0.0
 */
@Controller
public class HomeController {

    @RequestMapping("/home")
    public String index(Model modelo) {
        return "hello_jsp";
    }
}
