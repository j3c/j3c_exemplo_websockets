package br.com.j3c.websockets.controller;

import br.com.j3c.websockets.core.Aluno;
import br.com.j3c.websockets.core.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author carlos.oliveira
 * @version 1.0.0
 */
@Controller
@RequestMapping(value = "/aluno")
public class AlunoController {

    @Autowired
    private AlunoService service;

    @RequestMapping(value = "/cadastrarAluno", method = RequestMethod.POST)
    public @ResponseBody Aluno cadastrarAluno(@RequestBody Aluno aluno) {
        return this.service.addAluno(aluno);
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrar() {
        return "cadastrar_aluno";
    }

    @RequestMapping(value = "/listarJSP", method = RequestMethod.GET)
    public String listarAlunos(Model modelo) {
        modelo.addAttribute("alunos", this.service.getAlunos());
        return "listar_alunos";
    }
}