package br.com.j3c.websockets.core;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author carlos.oliveira
 * @since 2016-09-11
 */
@Service
public class AlunoService {

    private List<Aluno> alunos = Lists.newArrayList();

    public Aluno addAluno(Aluno aluno) {
        aluno.setDataInsersao(new Date());
        this.alunos.add(aluno);
        return aluno;
    }

    public List<Aluno> getAlunos() {
        return this.alunos;
    }

}
