package br.com.j3c.websockets.core;

import java.util.Date;

/**
 * @author carlos.oliveira
 * @since 2016-09-11
 */
public class Aluno {

    private String nome;
    private String curso;
    private Date dataInsersao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public Date getDataInsersao() {
        return dataInsersao;
    }

    public void setDataInsersao(Date dataInsersao) {
        this.dataInsersao = dataInsersao;
    }
}
