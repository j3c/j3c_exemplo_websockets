var app = angular.module('j3c');
app.controller('FormularioWebsocket', ['$scope', '$http', function($scope, $http) {

    $scope.aluno = {};
    $scope.alunos = {};

    $scope.cadastrar = function() {
        var parameter = JSON.stringify($scope.aluno);
        $http({
            method: 'POST',
            url: 'http://localhost:8080/java_app/aluno/cadastrarAluno',
            headers: {'Content-Type': 'application/json'},
            data: parameter
        }).success(function(data, status, headers, config) {
            console.log(data);
        }).error(function(data, status, headers, config) {
            console.log(data);
        });
    };
}]);