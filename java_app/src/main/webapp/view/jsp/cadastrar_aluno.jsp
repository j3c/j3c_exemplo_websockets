<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:url value="/resources/js/j3c.js" var="j3cJS"></spring:url>
<spring:url value="/resources/js/formularioWebsocket.js" var="formularioWebsocketJS"></spring:url>
<spring:url value="/resources/js/dist/angular-websocket.min.js.js" var="angularWebsocketJS"></spring:url>
<html>
<body ng-app="j3c">
	<div ng-controller="FormularioWebsocket">
		<label>Name:</label>
		<input type="text" ng-model="aluno.nome" />
		<label>Curso:</label>
		<input type="text" ng-model="aluno.curso" />
		<hr>
		<button ng-click="cadastrar()">
			Cadastrar
		</button>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
	<script src="${angularWebsocketJS}"></script>
	<script src="${j3cJS}"></script>
	<script src="${formularioWebsocketJS}"></script>
</body>
</html>